# pluralkit.xyz

A small website that lets you view some PluralKit data. https://pluralkit.xyz/

Not affiliated with PluralKit itself, despite the similar url :P

## Warning: This code is bad
It's a 2 year old project. I didn't fully know what I was doing 2 years ago. Proceed with caution.

## Building
This project uses [sveltekit](https://kit.svelte.dev/).

`pnpm` and `nodejs` (>=18.13) are required. Use `pnpm dev` to start a development server. Use `pnpm build` to build for production.

There's no dockerfile as of right now. This might change whenever I'm too tired of deploying it manually! Or you could open a PR.